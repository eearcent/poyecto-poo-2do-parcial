
package proyecto_poo_2;

import java.io.*;

/**
 *
 * @author Erika Arcentales
 */
public class Archivos {
    
    public String leerTxt(String direccion){//direccion el arichivo
        String texto = "";
        try{
            BufferedReader bf =  new BufferedReader(new FileReader(direccion));
            String tempo="";
            String bfRead;
            while((bfRead = bf.readLine())!= null){//ciclo de bf mientras tiene datos
                tempo = tempo +bfRead;
            }
            texto = tempo;
            
        }catch(IOException e){
            System.err.println("No se encontro ningun archivo");
            
        }
            
        return texto;        
    }
   
}
